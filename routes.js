const express = require("express")
const routes = express.Router()
const cors = require('cors');
routes.use(cors())

const UsuariosController = require("./controllers/UsuariosController")


routes.get("/usuarios", UsuariosController.index)